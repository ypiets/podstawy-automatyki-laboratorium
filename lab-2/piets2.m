delete(findall(0,'Type','figure')); %usuwanie wszystkich otwartych wykresow
%---------------------------------I----------------------------------------
%Uklad inercyjny I rzadu
clear;
k = 1;
T = 2;

licz = [0,k];
mian = [T,1];

figure;
subplot(1,2,1);
nyquist(licz, mian);
subplot(1,2,2);
bode(licz, mian);
%---------------------------------I----------------------------------------


%--------------------------------II----------------------------------------
%Uklad inercyjny II rz?du
clear;
k = 1;
T1 = 1;
T2 = 1;

licz = [0,0,k];
mian = [T1*T2 ,T1+T2 ,1];

figure;
subplot(1,2,1);
nyquist(licz, mian);
subplot(1,2,2);
bode(licz, mian);
%--------------------------------II----------------------------------------

%--------------------------------III---------------------------------------
%inercyjny II rz?du
clear;
k = 1;
T = 2;
ksi = 0.5;


%dla ksi < 1
licz = [0,0,k];
mian = [T^2 ,2*ksi*T ,1];

figure;
subplot(1,2,1);
nyquist(licz, mian);
subplot(1,2,2);
bode(licz, mian);


%dla ksi > 1
ksi = 2;
licz = [0,0,k];
mian = [T^2 ,2*ksi*T ,1];

figure;
subplot(1,2,1);
nyquist(licz, mian);
subplot(1,2,2);
bode(licz, mian);

%--------------------------------III---------------------------------------

%---------------------------------IV---------------------------------------
%calkujacy rzeczywisty
clear;
k = 1;
T = 2;
Ti = 99;

licz = [0,0,k];
mian = [T*Ti , Ti , 0];

figure;
subplot(1,2,1);
nyquist(licz, mian);
subplot(1,2,2);
bode(licz, mian);
%---------------------------------IV---------------------------------------

%----------------------------------V---------------------------------------
%roziczkujacy rzeczywisty
clear;
T = 2;
Td = 3;

licz = [Td,0];
mian = [T,1];

figure;
subplot(1,2,1);
nyquist(licz, mian);
subplot(1,2,2);
bode(licz, mian);
%----------------------------------V---------------------------------------

%--------------------------------VI----------------------------------------
%inercyjny I rz?du z opoznieniem
clear;
k = 1;
T = 2;
theta = 5; %op??nienie w sekundach
n = 5; %rz?d aproksymacji

licz_iner = [0 k];
mian_iner = [T 1];

[licz_op, mian_op] = pade(theta, n); %dokonanie aproksymacji Pade`go

[licz, mian] = series(licz_iner, mian_iner, licz_op, mian_op);

figure;
subplot(1,2,1);
nyquist(licz, mian);
subplot(1,2,2);
bode(licz, mian);
%--------------------------------VI----------------------------------------