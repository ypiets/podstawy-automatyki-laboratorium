function blad = ident( X0 )
%IDENT Summary of this function goes here
%   Detailed explanation goes here

a = X0(1);
b = X0(2);
theta = X0(3);
t = 1:10;
n = 5;

licz = [1];
mian = [a b 1];

s = tf('s');

Gr = 5 ^10 / (s+5)^10;

Gm = tf(licz, mian);
[liczp, mianp] = pade(theta, n);
Go = tf(liczp,mianp);

y_rzecz = step(Gr, t);
y_sym = step (series(Go,Gm), t);

e = y_rzecz - y_sym;
blad = sum(e.^2) / length(e);
end

