%--------------------------------------------%
clear;
T=1;
K=1;
theta=1;
a=0.35;
b=1.001;
n=5;

[licz, mian] = pade(theta, n);
s = tf('s');
G = 5^10 / (s+5)^10;
[num, dem] = tfdata(G);
num = num{1};
dem = dem{1};
%sim model2;
%blad wynosi 0.06173
%--------------------------------------------%

%----------Optymalizacja numeryczna----------%
[parametry, blad] = fminsearch('ident', [a,b,theta]); 
a = parametry(1);
b = parametry(2);
theta = parametry(3);

[licz, mian] = pade(theta, n);
sim model2;
%blad 0.05

%----------Optymalizacja numeryczna----------%