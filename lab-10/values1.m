clear;
theta = 1;
K =1;
T=1;

s = tf('s');

G1 = (1 - (theta * s )/2) / (1 + (theta * s )/2);
G2 = (1 - (theta * s )/2  + (theta^2  * s^2)/12) / (1 + (theta * s )/2  + (theta^2  * s^2)/12);

num1 = [-2 4];
den1 = [2 4];

num2 = [48 -288 576];
den2 = [48 288 576];