%-----------------------Klasyczny-----------------------%
G1 = zpk([],[-1 -1 -1],10);
G2 = zpk([],-2,3);

R = pidstd(1,1);
R = pidtune(G1,R,0.2);

sys_series = series(R*G1,G2);

sys_klas = feedback(sys_series,1);
%-----------------------Klasyczny-----------------------%

%-----------------------Kaskadowe-----------------------%
R2 = pidstd(1,1);
R2 = pidtune(G2,R2,2);

sys_pod = feedback(G2*R2,1);

R1 = pidstd(1,1);
R1 = pidtune(G1*sys_pod,R1,0.2);

sys_nad = feedback(G1*R1*sys_pod,1);

hold on;
step(sys_nad);
step(sys_klas);
hold off;
%-----------------------Kaskadowe-----------------------%