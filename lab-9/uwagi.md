**Uwagi**

Jednym ze sposobów do zasymulowania regulatorów PI R1 i R2 w Simulinku, jest

1.Utworzyć regulator podrzędny typu PI w Matlabie oraz nadać im znaczenie za pomocą funkcji ``pidtune`` :
```
R2 = pidstd(1,1);
R2 = pidtune(G2,R2,2);

R1 = pidstd(1,1);
R1 = pidtune(G1*sys_pod,R1,0.2);
```
2.Następnie w konsoli policzyć transmitancje dal R1 i R2:
```
>> tf(R1)

ans =
 
  0.01499 s + 0.02094
  -------------------
           s
 
Continuous-time transfer function.

>> tf(R2)

ans =
 
  0.244 s + 1.821
  ---------------
         s
 
Continuous-time transfer function.
```

3.W Simulinku użyć bloczka Transfer Fcn i wpisać odpowiednie wartosci otrzymane w p.2.