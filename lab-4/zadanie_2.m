delete(findall(0,'Type','figure')); %usuwanie wszystkich otwartych wykresow
clear;
%-a------------------------------------------------------------------------

s = tf('s');
Go = 1/(s*(s+1)*(0.2*s+1));

%{
locus(Go);
line([0 -1],[0 1]);
pause;
[K, bieg] = rlocfind(Go);
%}

K= 0.4245; %wartsoc znaleziona za pomaca powy?szych czterech linijek

Gz = feedback(K*Go, 1);
%-a------------------------------------------------------------------------

%-b------------------------------------------------------------------------

s = tf('s');
Gc = (s+1)/(0.1*s + 1);
Gz2 = series(Gc, Go);

%{
rlocus(Gz2);
line([0 -10],[0 10]);
pause;
[Kc, bieg] = rlocfind(Gz2);
%}
Kc = 1.6; %wartsoc znaleziona za pomaca powy?szych czterech linijek

Gz2 = feedback((series(Kc*Gc, Go)), 1);

hold on;
step(Gz);
step(Gz2);

%-b------------------------------------------------------------------------