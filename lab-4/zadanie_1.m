delete(findall(0,'Type','figure')); %usuwanie wszystkich otwartych wykresow
clear;


s = tf('s');

figure;
sgrid; %wyswietla linie  sta?ego t?umienia  (wychodz?cepromieni?cie z pocz?tku uk?adu wsp??rz?dnych) oraz linie  sta?ej cz?stotliwo?ci w?asnej uk?adu(elipsy o ?rodku w pocz?tku uk?adu wsp??rz?dnych).
subplot(3,1,1);
G1 = 1 / ((s+1)*(5*s+1));
rlocus(G1);

subplot(3,1,2);
G2 = (0.5*s + 1)/ ((s+1)*(5*s+1));
rlocus(G2);

subplot(3,1,3);
G3 = 1 / ((s+1)*(5*s+1)*(0.5*s + 1));
rlocus(G3);

%nale?y klikn?? mysz? w punkcie przeci?cia si? wykresu z osi? urojon?
%K - b?dzie w?wczas warto?ci? wzmocnienia tego punktu linii pierwiastkowej
%bieguny  - wektor b?dzie zawiera? warto?ci biegun?w dla tego wzmocnienia
[K, bieguny] = rlocfind(G3);

