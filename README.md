#Podstawy Automatyki - Laboratorium#

Lista rozwiąznych zadań z laboratorium

	1. Temat 1 - Reprezentacja układów liniowych niezmienniczych w czasie w Matlabie
	2. Temat 2 -	Charakterystyki czasowe podstawowych obiektów dynamicznych &&
					Charakterystyki częstotliwościowe podstawowych obiektów dynamicznyc
	3. Temat 3 - Identyfikacja obiektu regulacji
	4. Temat 4 - Metoda projektowania układów regulacji za pomocą linii pierwiastkowych
	5. Temat 5 - Metoda Zieglera-Nicholsa doboru nastaw regulatorów PID
	6. Temat 6 - Projektowanie układów regulacji – elementy korekcyjne
	7. Temat 9 - Sterowanie kaskadowe
	8. Temat 10 - Układy z opóźnieniem
	9. temat 11 - Linearyzacja układów nieliniowych