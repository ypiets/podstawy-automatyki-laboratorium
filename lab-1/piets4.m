clear;
z = [-1/4];
p = [0 -1/5 -1/10];
k = 4/50;

[licz, mian] = zp2tf(z,p,k);

[A1, B1, C1, D1] = zp2ss(z,p,k);
[A2, B2, C2, D2] = tf2ss(licz,mian);

step(A1, B1, C1, D1);
figure();
step(A2, B2, C2, D2);

k = dcgain(A,B,C,D);