clear;

licz1 = [0 0 1];
licz2 = [1 5 1];
mian1 = [0 0 0 1];
mian2 = [1 1 -2 1];

sys1 = tf(licz1, mian1);
sys2 = tf(licz2, mian2);

%po??czennie szeregowe 
sysSeries = series(sys1,sys2);
%po??czenie r?wnoleg?e
sysParallel = parallel(sys1,sys2);
%Sprz??enie zwrotne
sysFeedback = feedback(sys1,sys2);

%step(sysSeries);
%figure();
%step(sysParallel);
%figure();
%step(sysFeedback);

impulse(sysSeries);
figure();
impulse(sysParallel);
figure();
impulse(sysFeedback);