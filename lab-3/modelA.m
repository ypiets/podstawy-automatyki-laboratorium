delete(findall(0,'Type','figure')); %usuwanie wszystkich otwartych wykresow
%--------------Metoda 1 Obiekt inercyjny I rz?du z op??nieniem------------%
clear;
load obiekt;

K=2.05;
t=1:60;
%plot(t,y, 'r');

k=2.1516;
T=15.8018; %dane poprane prze ginput
theta=7.8954; %dane poprane prze ginput
t = 1:60;

[licz_op, mian_op]=pade(theta, 5);
licz_iner = [0,k];
mian_iner = [T,1];
%transmitancja ob iner. z op??nieniem
[licz, mian]=series(licz_op, mian_op, licz_iner, mian_iner); 
y_model = step(licz, mian, t);

figure;
plot(t,y,t,y_model);
grid on;
title(['K= ', num2str(K), ' T= ', num2str(T), ' theta= ', num2str(theta)]);

%--------------Metoda 1 Obiekt inercyjny I rz?du z op??nieniem------------%

%------------------Metoda 4 - Optymalizacja numeryczna--------------------%
clear;
load obiekt;
K0 = 2.1;
T0 = 14;
n0 = 9;

[parametry, blad] = fminsearch('identA', [K0,T0,n0]);

K = parametry(1);
T = parametry(2);
theta = parametry(3);

licz = [0 K];
mian = [T 1];

model = tf(licz, mian);
set(model, 'OutputDelay', theta);

t=1:60;
y_model = step(model,t);

figure;
plot(t,y,t,y_model);
grid on;
title(['K= ', num2str(K), ' T= ', num2str(T), ' e= ', num2str(blad)]);
%------------------Metoda 4 - Optymalizacja numeryczna--------------------%