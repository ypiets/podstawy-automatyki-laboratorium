function blad = identB_2(X0)
K = X0(1);
T1 = X0(2);
T2 = X0(3);
theta = X0(4);
t=1:60;

load obiekt;
y_rzecz = y;

s = tf('s');
G=K*exp(-s*theta)/((T1*s+1)*(T2*s+1));
[y_sym,x] = step(G,t);

e = y_rzecz - y_sym;
blad = sum(e.^2) / length(e);

