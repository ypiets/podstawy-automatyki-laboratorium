%ident dla obiektu I rzedu
function blad = identA(X0)
K = X0(1);
T = X0(2);
n = X0(3);

load obiekt;
y_rzecz = y;

licz = [0 K];
mian = [T 1];

model = tf(licz, mian);
set(model, 'OutputDelay', n);

t=1:60;
y_sym = step(model,t);

e = y_rzecz - y_sym;
blad = sum(e.^2) / length(e);