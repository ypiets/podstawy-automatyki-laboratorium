function blad = identB_1(X0)
K = X0(1);
ksi = X0(2);
theta = X0(3);

load obiekt;
y_rzecz = y;

T1 = (ksi + sqrt(ksi^2 - 1));
T2 = (ksi - sqrt(ksi^2 - 1));

licz = [0 0 K];
mian = [T1*T2, T1+T2, 1];

model = tf(licz, mian);
set(model, 'OutputDelay', theta);

t=1:60;
y_sym = step(model,t);

e = y_rzecz - y_sym;
blad = sum(e.^2) / length(e);