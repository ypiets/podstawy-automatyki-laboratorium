function blad = identC(X0)
load obiekt;
    
K = X0(1);
T = X0(2);
global n;

s=tf('s');
G = K/((T*s+1)^n);

t = 1:60;
y_model = step(G, t);
   
Ip=(sum((y_model-y).^2))/60;
blad=Ip;