delete(findall(0,'Type','figure')); %usuwanie wszystkich otwartych wykresow
clear;
load obiekt;

K0 = 2;
T0 = 7;
global n;
n = 3;

[parametry, blad] = fminsearch('identC',[2,7]);
K = parametry(1);
T = parametry(2);

s=tf('s');
G=K/((T*s+1)^n);

t = 1:60;
y_model = step(G, t);

plot(t, y, t, y_model);
grid on;
title(['K= ', num2str(K), ' T= ', num2str(T), ' n= ', num2str(n), ' e= ', num2str(blad)]);

