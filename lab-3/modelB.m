delete(findall(0,'Type','figure')); %usuwanie wszystkich otwartych wykresow
clear;
load obiekt;

%------------------Obiekt inercyjny II rzadu � metoda 1--------------------
clear;
load obiekt;
K = 2.05;
theta = 7.5;
t=1:60;

%{
hold on;
line([0 60], [K*0.714 K*0.714],'Color','red','LineStyle','--');
plot(t,y);
line([tb tb], [0 0.1], 'Color','red','LineStyle','--');
grid on;
hold off;
%}

ta = 26.5503; % wartosc z ginput
tb = ta / 4;
y_tb = 0.5;

T1 = ta / (1.2 + 1);
T2 = (ta - 1.2 * T1) / 1.2;

%{
s = tf('s');
G= K*exp(-s*theta)/((T1*s+1)*(T2*s+1));
[y_model, x] = step(G,t);
%}

[licz_op, mian_op]=pade(theta, 5);
licz_iner = [0,0,K];
mian_iner = [T1*T2, T1+T2, 1];
%transmitancja ob iner. z op??nieniem
[licz, mian]=series(licz_op, mian_op, licz_iner, mian_iner); 
y_model = step(licz, mian, t);


figure();
plot(t, y, t, y_model);
title(['K= ', num2str(K), ' T1= ', num2str(T1),' T2= ', num2str(T2), ' theta= ', num2str(theta)]);
%------------------Obiekt inercyjny II rz?du � metoda 1--------------------


%------------------Metoda 4.1 Optymalizacja numeryczna(ksi)----------------
clear;
load obiekt;
K = 2;
ksi = 6.5;
theta = 8;

[parametry, blad] = fminsearch('identB_1', [K,ksi,theta]);

K = parametry(1);
ksi = parametry(2);
theta = parametry(3);

T1 = (ksi + sqrt(ksi^2 - 1));
T2 = (ksi - sqrt(ksi^2 - 1));

licz = [0,0,K];
mian = [T1*T2 , T1 + T2, 1];

model = tf(licz, mian);
set(model, 'OutputDelay', theta);

t=1:60;
y_model = step(model,t);
figure;
plot(t, y, t, y_model);
title(['K= ', num2str(K), ' T1= ', num2str(T1),' T2= ', num2str(T2), ' ksi= ', num2str(ksi),' theta= ', num2str(theta)]);
%------------------Metoda 4.1 Optymalizacja numeryczna(ksi)----------------

%------------------Metoda 4.2 Optymalizacja numeryczna(T)----------------

clear;
load obiekt;
t=1:60;
K = 2.05;
T1 = 14;
T2 = 1.1;
theta = 7.5;

[parametry, blad] = fminsearch('identB_2', [K,T1,T2,theta]);

K = parametry(1);
T1 = parametry(2);
T2 = parametry(2);
theta = parametry(3);

s = tf('s');
G=K*exp(-s*theta)/((T1*s+1)*(T2*s+1));
[y_model,x] = step(G,t);

figure;
plot(t, y, t, y_model);
title(['K= ', num2str(K), ' T1= ', num2str(T1),' T2= ', num2str(T2), ' theta= ', num2str(theta), ' blad= ', num2str(blad)]);
%---------------Metoda 4.2 Optymalizacja numeryczna(T1, T2)----------------