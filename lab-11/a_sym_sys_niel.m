clear;
close all;

Q = 8000;   % [W] moc grzalki
w = 0.4;    % [kg/s] wyplyw
wi = 0.4;   % [kg/s] doplyw
Ti = 293;   % [K] temperatura cieczy doplywajacej

V0 = 0.04;  % [m3] objetosc poczatkowa
T0 = 293;   % [K] temperatura poczatkowa

%przekazanie funkcji stanu do solvera
[t,x] = ode45(@zbiornik_stan, [0 600], [V0,T0], [], wi, w, Ti, Q);


%Rysowanie
figure();
subplot(2,1,1);
plot(t, x(:,1));
xlabel('czas [s]');
ylabel('objetosc cieczy w zbiorniku [m3]');

subplot(2,1,2);
plot(t, x(:,2));
xlabel('czas [s]');
ylabel('temperatura [K]');