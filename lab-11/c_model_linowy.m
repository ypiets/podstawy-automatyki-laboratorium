close all;
clear;

%-------------------------Model Liniowy-------------------------%
% Wartosci poczatkowe
V0 = 0.04;
T0 = 293; 
wi = 0.4;
w = 0.4;
Ti = 293;
Q = 8000;

X0 = [0.04; 303]; % Wektor stanu
U0 = [wi; w; Ti; Q]; % Wektor wejsc
Y0 = [0.04; 303]; % Wektor wyjsc

% Blokowanie
IX = [];
IU = [1; 2; 3];
IY = [1; 2];

[x,u,y,dx] = trim('zbiornik_sys', X0, U0, Y0, IX, IU, IY);

% Linearyzacja
[A,B,C,D] = linmod('zbiornik_sys', x, u);

x0 = [V0, T0];
time = 0:1:600;
U = zeros([length(time), 4]);
for i = 1:1:length(time)
    U(i,:) = [wi; w; Ti; Q];
end;

[y,t] = lsim(A,B,C,D,U,time,x0);

% Porownanie
[t,x] = ode45(@zbiornik_stan, [0 600], [V0, T0], [], wi, w, Ti, Q);

figure();
plot(time, y(:,2), t, x(:,2));
legend('Model zlinearyzowany', 'Model nieliniowy');
xlabel('czas [s]');
ylabel('temperatura [K]');
%-------------------------Model Liniowy-------------------------%