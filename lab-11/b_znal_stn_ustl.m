%------------------Znalezie stanu ustalonego---------------------%
clear;
close all;

V0 = 0.04;
T0 = 293;

X0=[0.04;303];          % Wektor stanu
U0=[0.4;0.4;293;7000];  % Wektor wejsc
Y0=[0.04;303];          % Wektor wyjsc

%Blokowanie
IX = [];
IU = [1;2;3];
IY = [1;2];

[x,u,y,dx] = trim('zbiornik_sys',X0,U0,Y0,IX,IU,IY);

[t,x] = ode45(@zbiornik_stan, [0 600], y, [], u(1), u(2), u(3), u(4) );

figure();

subplot(2,1,1);
plot(t, x(:,1));
xlabel('czas [s]');
ylabel('obj?tosc cieczy w zbiorniku [m3]');;

subplot(2,1,2);
plot(t, x(:,2));
xlabel('czas [s]');
ylabel('temperatura [K]');
%------------------Znalezie stanu ustalonego---------------------%