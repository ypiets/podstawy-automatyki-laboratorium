function dx = zbiornik_stan(t,x,wi,w,Ti,Q)
% Argumenty wejsciowe:
% t - czas
% x - wektor stanu uk?adu
% wi - doplyw
% w - wyplyw
% Ti - temperatura cieczy dop;ywajacej
% Q - moc dostarczana (grzanie)
%----------------------------- zmienne stanu ------------------------------
x1 = x(1); %obe?tosc
x2 = x(2); %temperatura
%------------------------------- parametry --------------------------------
C = 1820; % cieplo wlasciwe [J/(Kg*K)]
ro = 1000; % gestosc [kg/m3]
%%---------------------------- r�wnania stanu -----------------------------
dx1 = 1 / ro * (wi-w);
dx2 = wi * (Ti - x2) / (ro * x1) + Q / (ro * x1 * C);
dx = [dx1;dx2];

