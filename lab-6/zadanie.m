delete(findall(0,'Type','figure')); %usuwanie wszystkich otwartych wykresow
clear;
%{
%zale?no?? przeregulowania (MO, maximum overshoot) i wsp??czynnika t?umienia ?
ezplot('100*exp((-ksi*pi)/sqrt(1-ksi^2))',[0 1]);
ylabel('MO [%]'), xlabel('\xi');
grid on;
%}

%przy MO=25%
ksi=0.4;    %wartosc odczytana za pomoca ginput

%{
%Zaleznosc zapasu fazy (PM, phase margin) i wspolczynnika tlumienia ?
ezplot('atand(2*ksi /  sqrt(sqrt(1+4*ksi^2) - 2*ksi^2))',[0 1]);
ylabel('PM [deg]'), xlabel('\xi');
grid on;
%}

%przy ksi=0.4, PM z kompensacja
PM_kom=39.3; %wartosc odczytana za pomoca ginput

s = tf('s');
G = 10/(s*(s+1));
%margin(G);
PM_bez=18;  %wartosc odczytana z wykresu margin(G)

%Zapas fazy
phi = PM_kom - PM_bez;

a = ((1-sind(phi))/(1+sind(phi)));

%dodatkowe wzmocniene wok?? cz?stotliwo?ci omega
A= -10*log10(a); %A=3.26

%wykres bodego uk?adu bez kompensacji
%bode(G);
omega = 2.53; %wartosc odczytana z wykresu dla A=3.26
T = 1/(omega*sqrt(a));

s = tf('s');

if(a > 1)
    Gc = (a*T*s + 1)/(T*s + 1);        %dla a>1
else
    Gc = (1/a)*((s + 1/T)/(s+1/(a*T))); %dla a < 1
end;

subplot(1,2,1);
hold on;
margin(G);
margin(Gc*G);
margin(Gc*Gc*G);
hold off;

subplot(1,2,2);
hold on;
hold off;

sys_bez = feedback(G,1);
sys_kom = feedback(G*Gc,1);

%nie s? spe?nione za?o?enia, bo MO = 40%, wi?c zgodnie z instrukcj? mo?na
%doda? drugi komensator
sys_komx2 = feedback(G*Gc*Gc,1);

subplot(1,2,2);
hold on;
step(sys_bez);
step(sys_kom);
step(sys_komx2);
legend('bez kompensatora', 'z kompensatorem', 'z 2 kompensatorami');
hold off;